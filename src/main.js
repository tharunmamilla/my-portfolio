import React from 'react'
//import ReactDOM from 'react-dom'
import { Switch, Route } from 'react-router-dom';
import Home from './components/home';
import About from './components/about';
import Contact from './components/contact';
import Project from './components/project';
import Resume from './components/resume';

//import { returnStatement } from '@babel/types';
const Main = () => (
    <Switch>
        <Route exact path="/home" component={Home} />
        <Route path="/about" component={About} />
        <Route path="/contact" component={Contact} />
        <Route path="/Project" component={Project} />
        <Route path="/resume" component={Resume} />
    </Switch>
)
export default Main; 