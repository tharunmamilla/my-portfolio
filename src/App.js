import { Component } from 'react';
import React from 'react'
//import ReactDOM from 'react-dom'
import './App.css';
import { Layout, Header, Navigation, Content, Drawer, Footer, FooterSection, FooterLinkList } from 'react-mdl';
import { Link } from 'react-router-dom'
import Main from './main';

class App extends Component {
  render() {
    return (
      <html>
        <body>
          <div className="content-inside">
            <Layout fixedHeader className="layout-color">
              <Header transparent className="header-color" title={<Link style={{ textDecoration: 'none', color: 'white' }} to="/home">my portfolio</Link>}>

                <Navigation className="nav">

                  <a href="/about">About</a>
                  <a href="/resume">Resume</a>
                  <a href="/project">project</a>
                  <a href="/contact">Contact</a>

                </Navigation>
              </Header>
              <Drawer className="draw" title={<Link style={{ textDecoration: 'none', color: 'black' }} to="/home">my portfolio</Link>}>
                <Navigation>
                  <a href="/home">Home</a>
                  <a href="/about">About</a>
                  <a href="/resume">Resume</a>
                  <a href="/project">project</a>
                  <a href="/contact">Contact</a>

                </Navigation>
              </Drawer>
              <Content>
                <div className="page-content" />
                <Main />
              </Content>
              <Footer class="footer" size="mini">
                <FooterSection class="section" type="center" logo="my portfolio">
                  <FooterLinkList class="link">
                    <a href="/contact">Contact Me</a>
                    <a href="#">Privacy & Terms</a>
                  </FooterLinkList>
                </FooterSection>

              </Footer>
            </Layout>
          </div>
        </body>
      </html>
    );
  }
}
export default App;
