import React, { Component } from 'react';
import { Grid, Cell } from 'react-mdl'
class About extends Component {

    render() {
        return (
            <div>
                <Grid className="landing-grid">
                    <Cell col={12}>
                        <img
                            src="images/tan.jpg"
                            alt="avatar"
                            className="avatar-img"
                        />
                    </Cell>
                </Grid>
            </div>
        );
    }
}

export default About;