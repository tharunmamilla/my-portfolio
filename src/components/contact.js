import React, { Component } from 'react';
import { Grid, Cell, List, ListItem, ListItemContent, } from 'react-mdl'

class Contact extends Component {

    render() {
        return (
            <div className="contact-body">
                <Grid className='contact-grid'>
                    <Cell col={6}>
                        <h2>tan</h2>
                        <img
                            src="images/tan.jpg"
                            alt="avatar"
                            style={{ height: '250px' }}

                        />
                        <p style={{ fontsize: '24px', margin: 'auto', paddingTop: '1em' }}>
                            "i just completed my bachelors degree in SRM UNIVERSITY. I am a self taught beginner Web Developer, currently diving deeper into React js. I believe that to be successful in life, one needs to be obsessive with their dreams and keep working towards them.",
                        </p>
                    </Cell>
                    <Cell col={6}>
                        <h2>contac me</h2>
                        <hr />
                        <div className="contact-list">
                            <List>
                                <ListItem>
                                    <ListItemContent style={{ fontSize: '25px' }} >
                                        <i className="fa fa-phone-square" aria-hidden="true" />
                                        (+91) 8074369722
                                        </ListItemContent>
                                </ListItem>
                                <ListItem>
                                    <ListItemContent style={{ fontSize: '25px' }} >
                                        <i className="fa fa-instagram" aria-hidden="true" />
                                        t_for_tan
                                        </ListItemContent>
                                </ListItem>
                                <ListItem>
                                    <ListItemContent style={{ fontSize: '25px' }} >
                                        <i className="fa fa-envelope" aria-hidden="true" />
                                        reddytharun101@gmail.com
                                        </ListItemContent>
                                </ListItem>
                                <ListItem>
                                    <ListItemContent style={{ fontSize: '25px' }} >
                                        <i className="fa fa-skype" aria-hidden="true" />
                                        my skype
                                        </ListItemContent>
                                </ListItem>
                            </List>
                        </div>
                    </Cell>
                </Grid>
            </div >
        );
    }
}

export default Contact;