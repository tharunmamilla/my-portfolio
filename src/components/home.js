import React, { Component } from 'react';
import { Grid, Cell } from 'react-mdl'
class Landing extends Component {

    render() {
        return (
            <div style={{ width: '100%', margin: 'auto' }}>
                <Grid className="landing-grid">
                    <Cell col={12}>
                        <img
                            src="images/tan.jpg"
                            alt="avatar"
                            className="avatar-img"
                        />


                        <div
                            className="banner-text">
                            <h1>I'am Tarun</h1>
                            <hr />
                            <p>HTML || VS Code || React JS || Java script</p>

                            <div className="social-links">
                                <a href="https://www.linkedin.com/in/tharun-reddy-b475b2168//" rel="noopener noreferrer" target="_blank">
                                    <i className="fa fa-linkedin-square" aria-hidden="true" />
                                </a>
                                <a href="https://github.com/Tharun69" rel="noopener noreferrer" target="_blank">
                                    <i className="fa fa-github-square" aria-hidden="true" />

                                </a>
                                <a href="https://www.facebook.com/tharun.tan" rel="noopener noreferrer" target="_blank">
                                    <i className="fa fa-facebook-square" aria-hidden="true" />


                                </a>
                                <a href="https://www.facebook.com/tharun.tan" rel="noopener noreferrer" target="_blank">
                                    <i className="fa fa-google-square" aria-hidden="true" />
                                </a>
                            </div>
                        </div>
                    </Cell>


                </Grid>

            </div>

        );
    }
}

export default Landing;